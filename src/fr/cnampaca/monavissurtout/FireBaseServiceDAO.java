package fr.cnampaca.monavissurtout;

import java.util.ArrayList;

import fr.cnampaca.monavissurtoutcontroller.LocationController;

public class FireBaseServiceDAO implements IDAO{

	
	private static ArrayList<Review> maListMock;
	
	public FireBaseServiceDAO() {
		
		maListMock = new ArrayList<Review>();
		
		maListMock= recuperation();
		
	}
	
	
	@Override
	public ArrayList<Review> recuperation() {
	
		//ici c est pour appeler le JSON du webService, donc le mock
		maListMock.add(new Review("Saint Savournin" , 43.24 , 5.31 , "Y fait froid là bas!" , 0));
		maListMock.add(new Review("Paris" , 48.51 , 2.21 , "Pas mal de monuments ici" , 0));
		maListMock.add(new Review("Marseille" , 43.17 , 5.22 , "The Place!" , 0));
		maListMock.add(new Review("Aix en Provence" , 43.31 , 5.27 , "La c'est The Place To Be" , 0));
		maListMock.add(new Review("La Bouilladisse" , 43.23 , 5.35 , "Le plus haut lieu de la planête!" , 0));
		maListMock.add(new Review("Jouques" , 43.28 , 5.38 , "Le village le plus pommer de la Terre!!!" , 0));
		maListMock.add(new Review("Salon" , 43.38 , 5.05 , "Chez les pacoulins" , 0));
		maListMock.add(new Review("Gardanne" , 43.27 , 5.28 , "Boulot" , 0));
		maListMock.add(new Review("Puyricard" , 43.34 , 5.24 , "Dans le nom y ricard alors c est un bon village" , 0));
	
		return maListMock;
		
	}

	@Override
	public ArrayList<Review> update() {
		// TODO Auto-generated method stub
		return recuperation();
	}


	@Override
	public ArrayList<Review> trie(ArrayList<Review> maListMock,
			LocationController controller) {
		// TODO Auto-generated method stub
		return null;
	}

}
