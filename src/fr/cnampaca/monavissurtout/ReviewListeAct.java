package fr.cnampaca.monavissurtout;

import java.util.ArrayList;
import fr.cnampaca.monavissurtout.R;
import android.os.Bundle;
import android.app.ListActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ReviewListeAct extends ListActivity implements OnItemClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_review_liste);

		
	ReviewListDAO laReview = new ReviewList();
	ArrayList<Review> laListe = laReview.recuperation();
	
	ListAdapter adapter = new monAdapter(this, laListe);
	
	ListView maliste = (ListView) findViewById(android.R.id.list);
		//inscription a mon adapter
		maliste.setAdapter(adapter);
		maliste.setOnItemClickListener(this);

		
}


@Override
public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	
	getMenuInflater().inflate(R.menu.review_list, menu);
	
	return true;
	
}


public void onItemClick(AdapterView<?> laListe, View cell, int position, long id) {

	String avis =(String) ((monAdapter) laListe.getAdapter()).getComm(position);
	String message = "L'avis est : " + avis;
	
	Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	
	//Intent i = new Intent(this , MapActivity.class);
	//startActivity(i);
	
}

}
