package fr.cnampaca.monavissurtout;


import java.util.List;

import fr.cnampaca.monavissurtout.R;
import android.content.Context;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

public class monAdapter implements ListAdapter{

		private Context context; //je veux y avoir accés que depuis cette classe
		private LayoutInflater inflater;
		private List<Review> list1;
	
	
	//mon constructeur
	public monAdapter(Context context, List<Review> laListe){
		
		this.context = context;
		//je lui dis de manipuler des objets liste avec des objets review en castant
		this.list1 = laListe; 
		this.inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}
	
	public String getComm(int position){
		
		return this.list1.get(position).comm;
		
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		
		Log.d("Adapter", "getCount()");
		//this.laList = ReviewList.getInstance();
		return this.list1.size();
	
	}

	
	@Override
	public Object getItem(int position) {
	// TODO Auto-generated method stub
		
		Log.d("Adapter", "getItem( " + position + ")");
		//this.laList = ReviewList.getInstance();
		return list1.get(position);
	
	}

	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
		
	}

	
	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 1;
		
	}

	
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View maCellule = convertView;
		
		if (maCellule == null) {
			
			inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			maCellule = inflater.inflate(R.layout.cellules, parent, false);
			 
		}
		
		TextView title = (TextView) maCellule.findViewById(R.id.nom);
		TextView longi = (TextView) maCellule.findViewById(R.id.coordlongi);
		TextView lati =(TextView) maCellule.findViewById(R.id.coordlati);
	    TextView content = (TextView) maCellule.findViewById(R.id.descrip);

		    title.setText("" + this.list1.get(position).title);
		    longi.setText("Longitude :" + this.list1.get(position).avisLongi);
		    lati.setText("Latitude :" +  this.list1.get(position).avisLati);
		    content.setText("Commentaire : " + this.list1.get(position).comm);

		return maCellule;
		 
	}

	
	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		
		return 1;
	
	}

	
	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		
		return true;
	
	}

	
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		
		Log.d("Adapter", "isEmpty()");
		
		//this.laList = ReviewList.getInstance();
		if (this.list1.size() > 0) {
			return false;
		}
		return true;
	}

	
	@Override
	public void registerDataSetObserver(DataSetObserver arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		
		return true;
	
	}

	
	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return true;
		
	}

}
