package fr.cnampaca.monavissurtout;

import java.util.ArrayList;

public interface ReviewListDAO {
	
	public ArrayList<Review> recuperation();
	
	public ArrayList<Review> lecture();


}
