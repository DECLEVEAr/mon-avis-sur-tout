package fr.cnampaca.monavissurtout;

import fr.cnampaca.monavissurtoutcontroller.LocationController;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{

//je me suis résoud a mettre la récupération et traitement GPS dans le main
		
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//j'instantie mon controller
		LocationController leContr =new  LocationController(this);
	
		
		//Récupére mon bouton 
		Button monBouton = (Button) findViewById(R.id.BoutonDepart);
		//mes variables pour l'affichage
		double longi = leContr.gotLongitude();
		double lati = leContr.gotLatitude();		

		
	    //linkage avec le layout
		TextView testAffiche = (TextView) findViewById(R.id.Affichepos);
		TextView timeAffiche= (TextView) findViewById(R.id.Timer);
		//alteration des textviews
			testAffiche.setText("la longuitude est :"+ longi +" et la latitude est :" +  lati );
			timeAffiche.setText(leContr.date());
		
		  //lorsque je change de localisation j affiche un taost!je le garde centré vers le bah au milieu
		
		String msg = "Bonjour vous êtes aux coordonnées : " + lati +" de latitude " + longi +" de longitude";
	 	Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
		
	 	
	 	monBouton.setOnClickListener(this);
	 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			Intent i = new Intent(this , ReviewListeAct.class);
			startActivity(i);
			
		}
	
	
}
