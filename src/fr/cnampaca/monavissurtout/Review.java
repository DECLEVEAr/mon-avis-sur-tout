package fr.cnampaca.monavissurtout;

public class Review {
	
	//c est pour le mock et n a d'existance que pour celui çi
	//avec un web service plus besoin de cette class
	
	//un avis a une longitude et une latitude
	// tout est en public pour les utiliser dans la class monAdapter, dans un autre paquetage
			public double avisLongi;
			public double avisLati;
			//Location coordo = (avisLongi, avisLati);
			//un avis a un commentaire
			public String comm;
			public String title;
			public double dist;
			
	//mon constructeur
	
	public Review(String nom , double longi , double lati , String commentaire , double distance){
		this.avisLongi = longi;
		this.avisLati = lati;
		this.comm = commentaire ;
		this.dist = distance;
	}
	
	
	public double getElemLongi(){
	
		return avisLongi;
	
	}
	
	
	public double getElemLati() {
		
		return avisLati;
		
	}
	
	public double setDistance(double distance){
		return this.dist = distance;
		
	}
}
