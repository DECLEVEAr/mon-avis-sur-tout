package fr.cnampaca.monavissurtout;

import java.util.ArrayList;

import fr.cnampaca.monavissurtoutcontroller.LocationController;

public interface IDAO {

	public ArrayList<Review> recuperation();
	
	public ArrayList<Review> update();

	public ArrayList<Review> trie(ArrayList <Review> maList , LocationController controller);
}
