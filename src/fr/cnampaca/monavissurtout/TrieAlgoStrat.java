package fr.cnampaca.monavissurtout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.location.Location;

import fr.cnampaca.monavissurtoutcontroller.LocationController;

public class TrieAlgoStrat {
//Algo pour trie, classe que l'on peut changer
	
	public IDAO listATrier;
	private ArrayList <Review> list;
	private LocationController controller;
	private Location location;
	
	Location loc1 = controller.getPosition(location);//pour ne pas a avoir appeler plusieurs fois
	
	
	public TrieAlgoStrat(){
		list = listATrier.recuperation();
		ranger(list);
	}
		


	
	public ArrayList <Review> ranger(ArrayList <Review> liste){
		
		Collections.sort(list, new Comparator<Review>() {
			
				public int compare(Review r1, Review r2){
					
					Location loc2 = new Location("pt1");
					loc2.setLatitude(r1.getElemLati());
					loc2.setLongitude(r1.getElemLongi());
					
					Location loc3 = new Location("pt2");
					loc3.setLatitude(r2.getElemLati());
					loc3.setLongitude(r2.getElemLongi());
					
				return (int)(r1.setDistance(loc1.distanceTo(loc2)) - r2.setDistance(loc1.distanceTo(loc3)));
				}
		});
		return liste;
		}	
		
}