package fr.cnampaca.monavissurtoutcontroller;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import fr.cnampaca.monavissurtout.DAOFilterProximity;
import fr.cnampaca.monavissurtout.IDAO;
import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;


public class LocationController implements LocationListener{
	
	
	protected String timeTemp; //accés uniquement dans le paquetage
	protected Context context;
	private LocationManager monContr;
	private Location location;
	private double longitude;
	private double latitude;

	
    public LocationController(Context context) { 

    	try {
    		 
             monContr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
             monContr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0, this);
                     Log.d("Network", "Network");
                     
                     if (monContr != null) {
                    	 
                    	 location = monContr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                         
                    	 if (location != null) {
                    		 
                             latitude = location.getLatitude();
                             longitude = location.getLongitude();
                             
                         }
                    	 
                     }
                     
                     if (location == null) {
                    	 
                         monContr.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0, this);
                         Log.d("GPS Enabled", "GPS Enabled");
                         
                         if (monContr != null) {
                        	 
                             location = monContr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            
                             if (location != null) {
                            	 
                                 latitude = location.getLatitude();
                                 longitude = location.getLongitude();
                                 
                             }
                             
                         }
                     
             }
  
         } catch (Exception e) {
             e.printStackTrace();
         }
    	
    } 
	
	//d'exploitation de la date
	
	@SuppressLint("SimpleDateFormat")
	public String date() {
	
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat jour = new SimpleDateFormat("dd-MM-yyyy");
		return timeTemp = jour.format(gc.getTime());	
	
	}	
	
	
	//me donne la longitude
	public double gotLongitude() {
		
		longitude = location.getLongitude();
	        return longitude;
		
		}

	
	//me donne la latitude
	public double gotLatitude() {
		
		  latitude = location.getLatitude();
			return latitude ;
			
		}

	
	  public Location getPosition(Location location){
		  
		return location;
		
	  }
	  
	  
	    @Override 
	    public void onLocationChanged(Location location) { 
	    	location.getLatitude();
	    	location.getLongitude();
	    
	    	
	    	IDAO nouveauTrie = new DAOFilterProximity();
	    	nouveauTrie.recuperation();
	    	
	    } 

	    
	    @Override 
	    public void onProviderDisabled(String provider) { 
	        // TODO Auto-generated method stub 

	    } 

	    @Override 
	    public void onProviderEnabled(String provider) { 
	        // TODO Auto-generated method stub 

	    } 

	    @Override 
	    public void onStatusChanged(String provider, int status, Bundle extras) { 
	        // TODO Auto-generated method stub 

	    }

}